#pragma once

#include <memory>
#include <type_traits>

#include <dune/common/std/type_traits.hh>

#include "localfunctioninterface.hh"
#include "legacyvtkfunction.hh"
#include "defaultvtkfunction.hh"

namespace Dune
{
  namespace Vtk
  {
    /// \brief A Vtk::LocalFunction is a function-like object that can be bound to a grid element
    /// an that provides an evaluate method with a component argument.
    /**
    * Stores internally a Vtk::LocalFunctionInterface object for the concrete evaluation.
    **/
    template <class GridView>
    class LocalFunction
    {
      using Self = LocalFunction;
      using Entity = typename GridView::template Codim<0>::Entity;
      using LocalCoordinate = typename Entity::Geometry::LocalCoordinate;

      template <class LF, class E>
      using HasBind = decltype(std::declval<LF>().bind(std::declval<E>()));

    public:
      /// Construct the Vtk::LocalFunction from any function object that has a bind(element) method.
      template <class LF,
        disableCopyMove<Self, LF> = 0,
        class = void_t<HasBind<LF,Entity>> >
      LocalFunction (LF&& lf)
        : localFct_(std::make_shared<LocalFunctionWrapper<GridView,LF>>(std::forward<LF>(lf)))
      {}

      /// Construct a Vtk::LocalFunction from a legacy VTKFunction
      LocalFunction (std::shared_ptr<VTKFunction<GridView> const> const& lf)
        : localFct_(std::make_shared<VTKLocalFunctionWrapper<GridView>>(lf))
      {}

      /// Allow the default construction of a Vtk::LocalFunction
      LocalFunction () = default;

      /// Bind the function to the grid entity
      void bind (Entity const& entity)
      {
        assert(bool(localFct_));
        localFct_->bind(entity);
      }

      /// Unbind from the currently bound entity
      void unbind ()
      {
        assert(bool(localFct_));
        localFct_->unbind();
      }

      /// Evaluate the `comp` component of the Range value at local coordinate `xi`
      double evaluate (int comp, LocalCoordinate const& xi) const
      {
        assert(bool(localFct_));
        return localFct_->evaluate(comp, xi);
      }

    private:
      std::shared_ptr<LocalFunctionInterface<GridView>> localFct_ = nullptr;
    };

  } // end namespace Vtk
} // end namespace Dune
